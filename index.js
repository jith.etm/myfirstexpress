var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/my_db');

var upload = multer();
var app = express();

// *******************initiation

// app.get('/hello', function(req, res) {
// 	res.send("Hello world");
// });

// app.post('/hello', function(req, res) {
// 	res.send("You just called the post method at '/hello'!\n");
// });

// curl -X POST "http://localhost:3000/hello"

// *******************Routing
// var things = require('./things.js')

//both index.js and things.js should be in same directory
// app.use('/things', things);

// *******************URL Building

// app.get('/:id', function(req, res){
//    res.send('The id you specified is ' + req.params.id);
// });

// app.get('/things/:name/:id', function(req, res) {
// 	// console.log(req)
//    res.send('id: ' + req.params.id + ' and name: ' + req.params.name);
// });

// app.get('/things/:id([0-9]{5})', function(req, res) {
// 	// console.log(req)
//    res.send('id: ' + req.params.id);
// });

// //Simple request time logger
// app.use(function(req, res, next){
//    console.log("A new request received at " + Date.now());
   
//    //This function call is very important. It tells that more processing is
//    //required for the current request and is in the next middleware
//    function route handler.
//    next();
// });

//Middleware function to log request protocol
// app.use('/things', function(req, res, next){
//    console.log("A request for things received at " + Date.now());
//    next();
// });

// // Route handler that sends the response
// app.get('/things', function(req, res){
//    res.send('Things');
// });

//First middleware before response is sent
// app.use(function(req, res, next){
//    console.log("Start");
//    next();
// });

// //Route handler
// app.get('/', function(req, res, next){
//    res.send("Middle");
//    next();
// });

// app.use('/', function(req, res){
//    console.log('End');
// });

// *******************Templating

app.set('view engine', 'pug');
app.set('views','./views');

// app.get('/first_template', function(req, res){
//    res.render('first_view');
// });

// app.get('/dynamic_view', function(req, res){
//    res.render('dynamic', {
//       name: "TutorialsPoint", 
//       url:"http://www.tutorialspoint.com"
//    });
// });

// // *******************Serving static files
// app.use(express.static('public'));
// app.use(express.static('images'));

// app.get('/static_file_test', function(req, res){
//    res.render('static_file');
// });

// ++++++++++++++++++++Form data
// for parsing application/json
// app.use(bodyParser.json()); 


// for parsing application/xwww-
// app.use(bodyParser.urlencoded({ extended: true })); 
//form-urlencoded

// for parsing multipart/form-data
// app.use(upload.array()); 

// app.get('/form', function(req, res){
//    res.render('form');
// });

// app.post('/', function(req, res){
//    console.log(req.body);
//    res.send("recieved your request!");
// });

// app.get('*', function(req, res) {
// 	res.send('sorry invalid url')
// });


// ********************database
var personSchema = mongoose.Schema({
   name: String,
   age: Number,
   nationality: String
});

var Person = mongoose.model("Person", personSchema);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/person', function(req, res){
   res.render('person');
});

app.post('/getPerson', function(req, res){
   var personInfo = req.body; //Get the parsed information
   console.log('person: ', req)
   
   if(!personInfo.name || !personInfo.age || !personInfo.nationality){
      res.render('show_message', {
         message: "Sorry, you provided worng info", type: "error"});
   } else {
      var newPerson = new Person({
         name: personInfo.name,
         age: personInfo.age,
         nationality: personInfo.nationality
      });
		
      newPerson.save(function(err, Person){
         if(err)
            res.render('show_message', {message: "Database error", type: "error"});
         else
            res.render('show_message', {
               message: "New person added", type: "success", person: personInfo});
      });
   }
});

app.listen(3000);